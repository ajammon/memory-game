import React, { useCallback, useEffect, useState } from 'react';
import Card from './card'
import './MemoryGame.scss';

interface ICard {
  id: number;
  value: number;
  selected: boolean;
}

export default function MemoryGame() {
  const [selectedCards, setSelectedCards] = useState<number[]>([]);
  const [cards, setCards] = useState<ICard[]>(defaultCards())
  const [disableCards, setDisableCards] = useState<boolean>(false);
  const [isCompleted, setIsCompleted] = useState<boolean>(false)

  function defaultCards() {
    let cardArray = []
    for(let x=1; x <= 24; x++) {
       cardArray.push({
          id: x,
          value: x % 2 ? ((x+1)/2) : (x/2),
          selected: false
        }) 
    }

    return randomizeCards(cardArray)
  }

  function randomizeCards(array:ICard[]) {
    const length = array.length;
    for (let i = length; i > 0; i--) {
      const randomIndex = Math.floor(Math.random() * i);
      const currentIndex = i - 1;
      const temp = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temp;
    }
    return array;
  }

  const handleCheck = useCallback(() => {
    setDisableCards(false);
    const [first, second] = selectedCards;

    if (cards[first].value === cards[second].value) {
      const filteredCards = cards.filter((card) => {
        return card.value !== cards[first].value
      })
      setCards(filteredCards)
      if(filteredCards.length === 0) {
        setIsCompleted(true)
      }
      setSelectedCards([]);
      return;
    } else {
      const tempCards = cards.map((card) => {
         card.selected = false
         return card
      })
      setCards(tempCards)
      setSelectedCards([]);
      return;
    }   
  },[cards, selectedCards])

  useEffect(() => {
    let timeout: number = 0;
    if (selectedCards.length === 2) {
      timeout = window.setTimeout(handleCheck, 800);
    }
    return () => {
      clearTimeout(timeout);
    };
  }, [selectedCards, handleCheck]);

  function handleCardSelection(id: number, index: number) {
    if(!cards[index].selected) {
      if (selectedCards.length === 1) {
        setDisableCards(true);
        setSelectedCards((prev) => [...prev, index]);
      } else {
        setSelectedCards([index]);
      }
    }

    const tempCards = cards.map((card) => {
      if(card.id === id) {
        card.selected = true
        return card
      } else {
        return card
      }
    })

    setCards(tempCards)
  }

  return (
    <div className={'game-container'}>
      <div className={'card-wrapper'}>
        {cards.map((card, index) => {
          return <Card key={card.id} cardValues={{id: card.id, value: card.value, selected: card.selected}} index={index} handleCardSelection={handleCardSelection} disableCard={disableCards}/>
        })}
        {isCompleted && (
          <p>YOU WIN!!!!!</p>
        )}
      </div>
    </div>
  );
}
