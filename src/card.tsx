import React from 'react';
import './card.scss'
import logo from './assets/logo.png'

type CardProps = {
  cardValues: {
    id: number;
    value: number;
    selected: boolean;
  },
  index: number;
  disableCard: boolean;
  handleCardSelection: (id: number, index: number) => void;
}

export default function Card(props: CardProps) {
  const {cardValues, handleCardSelection, index, disableCard} = props 
    
  function onRotate() {
    if(!disableCard) {
      handleCardSelection(cardValues.id, index)
    }
  }

  return (
    <div className={`card-container card ${!cardValues.selected ? 'rotated' : ''}`} onClick={onRotate}>
      <div className={'back'}>
        <img src={logo} alt='Back of card' className={'logo-content'}/>
      </div>
      <div className={'front'}>
        <p className={'card-content'}>{cardValues.value}</p>
      </div>
    </div>
  );
}

